import numpy as np
import torch.nn as nn
from ncps.wirings import AutoNCP
from ncps.torch import LTC
import pytorch_lightning as pl
import torch
import torch.utils.data as data
import os
import matplotlib.pyplot as plt
import seaborn as sns

from libs import training_data


# Path to results folder
# if not existing: please create the folder "NCP_training" within the results folder
path_to_results = os.path.join("results", "NCP_training")

# Load trajectory and reference data
# Gaimersheim
trajectory1 = os.path.join("data", "waypoints_20180810150607_bus_signals.csv")
reference1 = os.path.join("data", "reference_20180810150607_bus_signals.csv")
# Ingolstadt
trajectory2 = os.path.join("data", "waypoints_20190401145936_bus_signals.csv")
reference2 = os.path.join("data", "reference_20190401145936_bus_signals.csv")
# München
trajectory3 = os.path.join("data", "waypoints_20190401121727_bus_signals.csv")
reference3 = os.path.join("data", "reference_20190401121727_bus_signals.csv")

# Training: route 1,2 & 3 -> later split into training, test and validation sets with 80/10/10
trajectory_training = [trajectory1, trajectory2, trajectory3]
reference_training = [reference1, reference2, reference3]

# Preparation (normalization) of the data
trajectories, trajectories_normalized, references, references_normalized, max_values = \
    training_data.training_data(trajectory_training, reference_training, path_to_results)

# Split data for training, validation and test
data_x = torch.from_numpy(np.expand_dims(trajectories_normalized[:9000], axis=0)).double()
data_y = torch.from_numpy(np.expand_dims(references_normalized[:9000], axis=0)).double()
val_x = torch.from_numpy(np.expand_dims(trajectories_normalized[9000:10000], axis=0)).double()
val_y = torch.from_numpy(np.expand_dims(references_normalized[9000:10000], axis=0)).double()
test_x = torch.from_numpy(np.expand_dims(trajectories_normalized[10000:], axis=0)).double()
test_y = torch.from_numpy(np.expand_dims(references_normalized[10000:], axis=0)).double()


def plot_training_data(inputs, targets):
    # Let's visualize the training data
    plt.figure(figsize=(12, 8))
    plt.plot(inputs[0, :, 0][:300], label="Input " r'$\Delta x(t)$')
    plt.plot(inputs[0, :, 1][:300], label="Input " r'$\Delta y(t)$')
    plt.plot(inputs[0, :, 2][:300], label="Input " r'$\Delta v(t)$')
    plt.plot(inputs[0, :, 3][:300], label="Input " r'$\Delta x(t+1)$')
    plt.plot(inputs[0, :, 4][:300], label="Input " r'$\Delta y(t+1)$')
    plt.plot(inputs[0, :, 5][:300], label="Input " r'$\Delta v(t+1)$')
    plt.plot(targets[0, :, 0][:300], label="Target " r'$\delta$')
    plt.plot(targets[0, :, 1][:300], label="Target " r'$a$')
    plt.ylim((-1, 1))
    plt.xlabel("Time [s]")
    plt.ylabel("y [-]")
    plt.title("Training data")
    plt.legend(loc="upper right")
    plt.savefig(os.path.join(path_to_results, "section_training_data.pdf"), dpi=900, format='pdf')


# Plot the training data
plot_training_data(data_x, data_y)


# LightningModule for training a RNNSequence module
class SequenceLearner(pl.LightningModule):
    def __init__(self, model, lr=0.005):
        super().__init__()
        self.model = model
        self.lr = lr

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat, _ = self.model.forward(x)
        loss = nn.MSELoss()(y_hat, y)
        self.log("train_loss", loss, prog_bar=True)
        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat, _ = self.model.forward(x)
        loss = nn.MSELoss()(y_hat, y)
        self.log("val_loss", loss, prog_bar=True)
        return loss

    def test_step(self, batch, batch_idx):
        # Here we just reuse the validation_step for testing
        return self.validation_step(batch, batch_idx)

    def configure_optimizers(self):
        return torch.optim.Adam(self.model.parameters(), lr=self.lr)


# Input data: delta x(t), delta y(t), delta v(t), delta x(t+1), delta y(t+1), delta v(t+1)
in_features = 6
# Output data: delta(t), acceleration(t)
out_features = 2

# num_workers can be changed according to used hardware
trainloader = data.DataLoader(
    data.TensorDataset(data_x, data_y), batch_size=1, shuffle=False, num_workers=0
)
valloader = data.DataLoader(
    data.TensorDataset(val_x, val_y), batch_size=1, shuffle=False, num_workers=0
)
testloader = data.DataLoader(
    data.TensorDataset(test_x, test_y), batch_size=1, shuffle=False, num_workers=0
)


def plot_network(wiring, numb_neurons):
    sns.set_style("white")
    plt.figure(figsize=(12, 12))
    legend_handles = wiring.draw_graph(draw_labels=False,  neuron_colors={"command": "tab:cyan"})
    plt.legend(handles=legend_handles, loc="lower center", ncol=2)
    sns.despine(left=True, bottom=True)
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "ncp_network_{}.pdf".format(numb_neurons)), dpi=900, format='pdf')


def plot_test_results(model, numb_neurons, lr, grad_clip):
    with torch.no_grad():
        prediction = model(test_x)[0].numpy()

    plt.figure(figsize=(16, 8))
    plt.plot(test_y[0, :, 0], label="Target")
    plt.plot(prediction[0, :, 0], label="NCP")
    plt.ylim((-1, 1))
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel(r'$\delta$ ' r"$[-]$")
    plt.title("Steering angle prediction after training")
    plt.legend(loc="upper right")
    plt.savefig(os.path.join(path_to_results, "prediction_delta_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf')

    plt.figure(figsize=(16, 8))
    plt.plot(test_y[0, :, 1], label="Target")
    plt.plot(prediction[0, :, 1], label="NCP")
    plt.ylim((-1, 1))
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel(r'$a$ ' r"$[-]$")
    plt.title("Acceleration prediction after training")
    plt.legend(loc="upper right")
    plt.savefig(os.path.join(path_to_results, "prediction_a_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf')


# Parameters for grid search: numb_neurons, lr and grad_clip
# Range of values for each parameter (can be changed)
grid_search = False
if grid_search:
    numb_neurons_list = [6, 32, 64]
    lr_list = [0.05, 0.01, 0.001]
    grad_clip_list = [0.1, 1.0, 10.0]
else:  # grid_search = false -> training with one parameter setting
    numb_neurons_list = [32]
    lr_list = [0.01]
    grad_clip_list = [1.0]


for numb_neurons in numb_neurons_list:
    for lr in lr_list:
        for grad_clip in grad_clip_list:
            # Initialization of the network
            wiring = AutoNCP(numb_neurons, out_features)
            ltc_model = LTC(in_features, wiring, batch_first=True, return_sequences=True)

            # Plot network architecture
            # plot_network(wiring, numb_neurons)

            learn = SequenceLearner(ltc_model, lr=lr)
            trainer = pl.Trainer(
                logger=pl.loggers.CSVLogger(
                    os.path.join(path_to_results, "log_{}_{}_{}".format(numb_neurons, lr, grad_clip))),  # Logg the loss and hyperparameter
                log_every_n_steps=1,
                max_epochs=500,
                gradient_clip_val=grad_clip,  # Clip gradient to stabilize training
                gpus=0,
            )

            # Train the model
            trainer.fit(learn, trainloader, valloader)

            # Test the model
            results = trainer.test(learn, testloader)

            # Plot the prediction results with the test data
            plot_test_results(ltc_model, numb_neurons, lr, grad_clip)




