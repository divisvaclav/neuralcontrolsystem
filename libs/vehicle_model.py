import numpy as np


# Initial values from for time stamp in test data
x = 311.1365150457714
y = -498.0309052
psi = np.radians(-60.0)
v = 7.38232891


def single_track_model(delta, a):
    # x = [p1, p2, psi, v]
    # p1: x-position [m]
    # p2: y-position [m]
    # psi: yaw angle [rad]
    # v: vehicle velocity [m/s]
    # delta: steering angle [rad]
    # a: acceleration [m/s^2]

    global x
    global y
    global psi
    global v

    dt = 0.1

    # CoG distance to front and rear axis
    lf = 3.0 / 2  # [m]
    lr = 3.0 / 2  # [m]

    # Calculate side slip angle beta
    beta = np.arctan2((np.tan(delta) * lr), (lf + lr))

    # Differential equations for:  p1_d, p2_d, psi_d, a -> p1, p2, psi, v
    v += a * dt
    psi += np.tan(delta) / (lf + lr) * v * dt
    while psi > np.pi:
        psi -= 2.0 * np.pi
    while psi < -np.pi:
        psi += 2.0 * np.pi
    x += v * np.cos(psi + beta) * dt
    y += v * np.sin(psi + beta) * dt

    state = np.array([x, y, psi, v])

    return state
