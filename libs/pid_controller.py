import numpy as np

e_i = 0
e_prev = 0

def PID(waypoint, state):
    k_p = 3
    k_i = 0.1
    k_d = 0.001
    global e_i
    global e_prev

    v_target = waypoint[2]

    dt = 0.1

    e = v_target - state[3]

    e_i += e * dt
    e_d = (e - e_prev) / dt

    a = k_p * e + k_i * e_i + k_d * e_d

    # Max. acceleration: +- 4 m/s^2 -> clip to max. value
    a = np.clip(a, -4.0, 4.0)

    # update stored data for next iteration
    e_prev = e

    return a, e
