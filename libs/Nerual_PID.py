import os
import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets, transforms

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class NeuralNetwork(nn.Module):
    def __init__(self, input_window, output_window, activation):
        super(NeuralNetwork, self).__init__()
        self.linear_stack = nn.Sequential(
            nn.Linear(input_window, input_window),
            activation,
            nn.Linear(input_window, output_window),
            activation,
            nn.Linear(output_window, output_window),
        )

    def forward(self, x):
        return self.linear_stack(x)

import torch.optim as optim

class EvolutionAlgorithm():
    def __init__(self):
        self.award = 0
        self.layers_space = [nn.Linear()]
        self.activation_space = [nn.Relu(), nn.LeakyReLU(), nn.Sigmoid(), nn.Tanh()]
        self.min_input = 16
        self.max_input = 128
        self.input_window = self.min_input + self.max_input
        self.criterion = nn.MSELoss()
        self.optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

    def training(self):
        net = NeuralNetwork(self.input_window, self.input_window, self.activation_space[0])
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = self.criterion(outputs, labels)
            loss.backward()
            self.optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0

print('Finished Training')

