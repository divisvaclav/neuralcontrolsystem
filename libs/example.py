# NCP Example with sine and cosine wave take from GitHub:
# https://colab.research.google.com/drive/1VWoGcpyqGvrUOUzH7ccppE__m-n1cAiI?usp=sharing

import numpy as np
import torch.nn as nn
from ncps.wirings import AutoNCP
from ncps.torch import LTC
import pytorch_lightning as pl
import torch
import torch.utils.data as data
import matplotlib.pyplot as plt
import os
import pandas as pd


# LightningModule for training a RNNSequence module
class SequenceLearner(pl.LightningModule):
    def __init__(self, model, lr=0.005):
        super().__init__()
        self.model = model
        self.lr = lr

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat, _ = self.model.forward(x)
        y_hat = y_hat.view_as(y)
        loss = nn.MSELoss()(y_hat, y)
        self.log("train_loss", loss, on_epoch=True, prog_bar=True, logger=True)
        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat, _ = self.model.forward(x)
        y_hat = y_hat.view_as(y)
        loss = nn.MSELoss()(y_hat, y)

        self.log("val_loss", loss, prog_bar=True)
        return loss

    def test_step(self, batch, batch_idx):
        # Here we just reuse the validation_step for testing
        return self.validation_step(batch, batch_idx)

    def configure_optimizers(self):
        return torch.optim.NAdam(self.model.parameters(), lr=self.lr)


# path to results folder
path = "../results"
# if not existing: please create the folder "example" within the results folder
path_to_results = os.path.join(path, "example")

in_features = 2
out_features = 1
N = 48  # Length of the time-series
batch_size = 1

# Input feature is a sine and a cosine wave
data_x = np.stack(
    [np.sin(np.linspace(0, 3 * np.pi, N)), np.cos(np.linspace(0, 3 * np.pi, N))], axis=1
)
data_x = np.expand_dims(data_x, axis=0).astype(np.float32)  # Add batch dimension

# Target output is a sine with double the frequency of the input signal
data_y = np.sin(np.linspace(0, 6 * np.pi, N)).reshape([1, N, 1]).astype(np.float32)

data_x = torch.Tensor(data_x)
data_y = torch.Tensor(data_y)
dataloader = data.DataLoader(
    data.TensorDataset(data_x, data_y), batch_size=1, shuffle=True, num_workers=0
)

# Visualize the training data
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
fig.tight_layout(pad=2.0)
fig.subplots_adjust(top=0.9)
ax1.plot(data_x[0, :, 0], label="Sine")
ax1.plot(data_x[0, :, 1], label="Cosine")
ax2.plot(data_y[0, :, 0], label="Sine")
ax1.grid()
ax2.grid()
ax1.set_ylim((-1, 1))
ax2.set_ylim((-1, 1))
ax1.set_xlim([0, 47])
ax2.set_xlim([0, 47])
ax2.set_xlabel("Time [s]")
ax1.set_ylabel("y [-]")
ax2.set_ylabel("y [-]")
ax1.set_title("Input Waves")
ax2.set_title("Target Wave")
ax1.legend(loc="upper right")
ax2.legend(loc="upper right")
fig.savefig(os.path.join(path_to_results, "training_data_example.pdf"), dpi=900, format='pdf', bbox_inches='tight')

# Initialize the NCP model
wiring = AutoNCP(16, out_features)  # 16 units, 1 motor neuron

ltc_model = LTC(in_features, wiring, batch_first=True)

learn = SequenceLearner(ltc_model, lr=0.01)
trainer = pl.Trainer(
    logger=pl.loggers.CSVLogger(path_to_results, name="loss"),  # Logg the loss and hyperparameter
    max_epochs=400,
    gradient_clip_val=1,  # Clip gradient to stabilize training
    gpus=0,
)

# Train the model
trainer.fit(learn, dataloader)

# Plot loss
path_to_loss = os.path.join(path_to_results, "loss", "version_0", "metrics.csv")
loss = pd.read_csv(path_to_loss)
train_loss = loss.train_loss_epoch
train_loss = train_loss.dropna()

# Plot training loss
epochs = np.arange(0, 400, 1)
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
plt.plot(epochs, train_loss, label="Loss")
plt.grid()
plt.xlim(0,399)
plt.ylabel("Loss " r"$[-]$")
plt.xlabel("Epochs " r"$[-]$")
plt.title("Training Loss")
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "training_loss_example.pdf"), dpi=900, format='pdf')

results = trainer.test(learn, dataloader)

# Visualize the results
with torch.no_grad():
    prediction = ltc_model(data_x)[0].numpy()

plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
plt.plot(data_y[0, :, 0], label="Target")
plt.plot(prediction[0, :, 0], label="Prediction")
plt.grid()
plt.ylim((-1, 1))
plt.xlim((0, 47))
plt.xlabel("Time [s]")
plt.ylabel("y [-]")
plt.title("Sine Wave")
plt.legend(loc="upper right")
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "prediction_example.pdf"), dpi=900, format='pdf')
