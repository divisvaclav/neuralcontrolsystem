import numpy as np


def stanley(waypoints, state):
    psi = state[2]
    v = state[3]
    lf = 3.0 / 2

    k = 0.1
    k_v = 0.1

    x_offset = np.sin(np.pi/2 - psi) * lf
    y_offset = np.cos(np.pi/2 - psi) * lf
    current_xy = np.transpose(np.array([state[0] + x_offset, state[1] + y_offset]))

    # Heading error psi_e
    psi_path = np.arctan2(waypoints[-1][1] - waypoints[0][1], waypoints[-1][0] - waypoints[0][0])
    psi_e = psi_path - psi
    if psi_e > np.pi:
        psi_e -= 2 * np.pi
    elif psi_e < - np.pi:
        psi_e += 2 * np.pi

    # Cross track error
    crosstrack_error = np.min(np.sqrt(np.sum((current_xy - np.array(waypoints)[:, :2]) ** 2, axis=1)))

    psi_cross_track = np.arctan2(current_xy[1] - waypoints[0][1], current_xy[0] - waypoints[0][0])
    psi_path2ct = psi_path - psi_cross_track
    if psi_path2ct > np.pi:
        psi_path2ct -= 2 * np.pi
    elif psi_path2ct < - np.pi:
        psi_path2ct += 2 * np.pi
    if psi_path2ct > 0:
        crosstrack_error = abs(crosstrack_error)
    else:
        crosstrack_error = - abs(crosstrack_error)

    # Steering angle
    delta = psi_e + np.arctan(k * crosstrack_error / (v + k_v))
    if delta > np.pi:
        delta -= 2 * np.pi
    elif delta < - np.pi:
        delta += 2 * np.pi

    # Maximum steering angle: 35°
    delta_max = np.deg2rad(35)
    delta = np.clip(delta, -delta_max, delta_max)  # Clip steering angle to max. value

    return delta
