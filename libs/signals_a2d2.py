import json
import os.path
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import utm


def signal_processing(data_path, result_path, name):
    # Load bus signals from A2D2 data
    json_file = open(data_path, 'r')
    file = json.load(json_file)

    # Load signals for gps_lat, gps_long, v, steering angle and steering angle sign
    gps_lat = np.asarray(file["latitude_degree"]["values"])[:, 1]  # [degree]
    gps_long = np.asarray(file["longitude_degree"]["values"])[:, 1]  # [degree]
    vehicle_speed = np.asarray(file["vehicle_speed"]["values"])[:, 1]  # [km/h]
    steering_angle = np.asarray(file["steering_angle_calculated"]["values"])[:, 1]  # [degree]
    steering_angle_sign = np.asarray(file["steering_angle_calculated_sign"]["values"])[:, 1]  # [0, 1]
    index = np.where(steering_angle_sign == 1)  # steering to the right == 1 (mathematical negative)
    steering_angle[index] = -steering_angle[index]  # add steering angle sign to steering angle value

    # Load time stamps for each signal [micro sec]
    gps_lat_t = np.asarray(file["latitude_degree"]["values"])[:, 0]
    gps_long_t = np.asarray(file["longitude_degree"]["values"])[:, 0]
    vehicle_speed_t = np.asarray(file["vehicle_speed"]["values"])[:, 0]
    steering_angle_t = np.asarray(file["steering_angle_calculated"]["values"])[:, 0]

    # Find min and max time which are included in all signals
    t1 = max(np.min(gps_lat_t), np.min(gps_long_t), np.min(vehicle_speed_t), np.min(steering_angle_t))
    t2 = min(np.max(gps_lat_t), np.max(gps_long_t), np.max(vehicle_speed_t), np.max(steering_angle_t))
    # t_tot = (t2 - t1) / 1000000  # (from micro sec to sec --> divide by 1e+6)# total time in sec

    # Interpolate the signals between t1 and t2 with dt = 0.1s (-> steps = 1e+6 / 10 for np.arange)
    dt = 0.1
    t_interp = np.arange(t1, t2, 100000)  # time span from t1 to t2 with 0.1 sec between the points (1e+5)
    gps_lat_interp = np.interp(t_interp, gps_lat_t[::5], gps_lat[::5])
    gps_long_interp = np.interp(t_interp, gps_long_t[::5], gps_long[::5])
    v_interp = np.interp(t_interp, vehicle_speed_t, vehicle_speed)
    steering_angle_interp = np.interp(t_interp, steering_angle_t, steering_angle)

    # Calculate velocity in m/s
    v = v_interp * 1000 / 3600

    # Calculate the acceleration in m/s^2 with the derivative of the velocity (a = (v(t+1) - v(t)) / dt)
    acceleration = np.diff(v, append=v[-1]) / dt

    # Convert steering angle from [°] to [rad]
    delta = np.pi / 180 * steering_angle_interp

    # Calculate position in cartesian coordinates [m]
    x, y, _, _ = utm.from_latlon(gps_lat_interp, gps_long_interp)
    # Shift positions so that they start in (0,0)
    x_centered = x - x[0]
    y_centered = y - y[0]

    # Let the time start at 0
    t = t_interp - t_interp[0]

    plot_data = False
    if plot_data:
        t_plot = np.arange(0, t.size / 10, 0.1)

        if idx == 0:
            r = 1
        elif idx == 1:
            r = 2
        elif idx == 2:
            r = 3

        # Plot the preprocessed data for each route
        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, x, linewidth=1)
        plt.grid()
        plt.title("x Position - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("x " r"$[m]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_x_{}.pdf".format(r)), dpi=900, format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, y, linewidth=1)
        plt.grid()
        plt.title("y Position - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("y " r"$[m]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_y_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, x_centered, linewidth=1)
        plt.grid()
        plt.title("x Position - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("x " r"$[m]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_centered_x_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, y_centered, linewidth=1)
        plt.grid()
        plt.title("y Position - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("y " r"$[m]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_centered_y_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(x, y, linewidth=1)
        plt.grid()
        plt.title("Path - Route {}".format(r))
        plt.xlabel("x " r"$[m]$")
        plt.ylabel("y " r"$[m]$")
        plt.savefig(os.path.join(result_path, "raw_path_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(x_centered, y_centered, linewidth=1)
        plt.grid()
        plt.title("Path - Route {}".format(r))
        plt.xlabel("x " r"$[m]$")
        plt.ylabel("y " r"$[m]$")
        plt.savefig(os.path.join(result_path, "raw_centered_path_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, v, linewidth=1)
        plt.grid()
        plt.title("Velocity - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("v " r"$[m/s]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_v_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, acceleration, linewidth=1)
        plt.grid()
        plt.title("Acceleration - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("a " r"$[m/s^2]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_acceleration_{}.pdf".format(r)), dpi=900,
                    format='pdf')

        plt.clf()
        plt.figure(1, figsize=(6, 4))
        plt.plot(t_plot, delta, linewidth=1)
        plt.grid()
        plt.title("Steering Angle - Route {}".format(r))
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel(r'$\delta$ ' r"$[m/s^2]$")
        plt.xlim([0, t.size / 10])
        plt.xticks(np.arange(0, t.size/10 + 60, 60))
        plt.savefig(os.path.join(result_path, "raw_steering_angle_{}.pdf".format(r)), dpi=900,
                    format='pdf')

    # Join x, y and v into one trajectory array and save it
    trajectory = np.transpose(np.vstack((x, y, v, t)))
    fname1 = 'waypoints_' + name + '.csv'
    path1 = os.path.join(result_path, fname1)
    pd.DataFrame(trajectory).to_csv(path1, index_label="Index", header=['x', 'y', 'v', 't'])

    # Join delta and acceleration into one trajectory array and save it
    reference = np.transpose(np.vstack((delta, acceleration)))
    fname2 = 'reference_' + name + '.csv'
    path2 = os.path.join(result_path, fname2)
    pd.DataFrame(reference).to_csv(path2, index_label="Index", header=['delta', 'a'])

    return trajectory, reference


path_to_data = "../data"
names = ["20180810150607_bus_signals", "20190401145936_bus_signals", "20190401121727_bus_signals"]

# Gaimersheim
name1 = names[0] + ".json"
route1 = os.path.join(path_to_data, name1)
# Ingolstadt
name2 = names[1] + ".json"
route2 = os.path.join(path_to_data, name2)
# München
name3 = names[2] + ".json"
route3 = os.path.join(path_to_data, name3)

routes = [route1, route2, route3]

for idx, route in enumerate(routes):
    trajectory, reference = signal_processing(route, path_to_data, names[idx])




