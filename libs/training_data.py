import numpy as np
import pandas as pd
import os.path
import matplotlib.pyplot as plt


def training_data(trajectory_signals, reference_signals, path_to_results):
    trajectories_normalized_list = list()
    trajectories_list = list()
    references_normalized_list = list()
    references_list = list()
    cut_index_list = list()

    delta_x_max_list = list()
    x_min_list = list()
    diff_x_list = list()
    delta_y_max_list = list()
    y_min_list = list()
    diff_y_list = list()
    v_max_list = list()
    diff_v_list = list()
    delta_max_list = list()
    a_max_list = list()

    # Find maximum values of each trajectory signal (needed for normalization)
    for index, item in enumerate(trajectory_signals):
        data = pd.read_csv(item)

        x = data.x.to_numpy()
        delta_x_max_list.append(np.max(x) - np.min(x))
        x_min_list.append(np.min(x))
        diff_x_list.append(np.max(np.abs(np.diff(x))))

        y = data.y.to_numpy()
        delta_y_max_list.append(np.max(y) - np.min(y))
        y_min_list.append(np.min(y))
        diff_y_list.append(np.max(np.abs(np.diff(y))))

        v = data.v.to_numpy()
        v_max_list.append(np.max(np.abs(v)))
        diff_v_list.append(np.max(np.abs(np.diff(v))))

    # Take only the maximum value over all routes
    delta_xy_max = max(delta_x_max_list + delta_y_max_list)
    diff_xy_max = max(diff_x_list + diff_y_list)
    v_max = np.max(v_max_list)
    diff_v_max = np.max(diff_v_list)

    # Same for the reference
    for index, item in enumerate(reference_signals):
        data = pd.read_csv(item)

        delta_max_list.append(np.max(np.abs(data.delta.to_numpy())))

        a_max_list.append(np.max(np.abs(data.a.to_numpy())))

    delta_max = np.max(delta_max_list)
    a_max = np.max(a_max_list)

    # Combine all max. values in one array
    max_values = np.asarray([delta_xy_max, diff_xy_max, v_max, diff_v_max, delta_max, a_max])

    # Open cvs file and extract the data -> loop through all routes and normalize the signals
    for index, item in enumerate(trajectory_signals):
        data = pd.read_csv(item)

        x = data.x.to_numpy()
        y = data.y.to_numpy()
        v = data.v.to_numpy()
        t = data.t.to_numpy()

        # Find all indices where the velocity is not zero and the vehicle changes its position (standing times not needed for training)
        delta_x = np.abs(np.diff(x, prepend=x[0]))
        delta_y = np.abs(np.diff(y, prepend=y[0]))
        cut_index = np.where(np.logical_not(np.logical_and(np.logical_and((delta_x <= 0.001), (delta_y <= 0.001)), (v <= 0.001))))
        cut_index_list.append(cut_index)

        # Cut out all indices where the velocity is zero
        x_cut = x[cut_index]
        y_cut = y[cut_index]
        v_cut = v[cut_index]
        t_cut = t[0:cut_index[0].size]
        t_plot = np.arange(0, t_cut.size / 10, 0.1)

        x_cut = x_cut - x_cut[0]  # Shift starting position to 0
        x_norm = x_cut / delta_xy_max  # Normalize position

        delta_x_cut = np.diff(x_cut, prepend=x_cut[0])  # Calculate difference between two successive points
        delta_x_norm = delta_x_cut / diff_xy_max

        y_cut = y_cut - y_cut[0]
        y_norm = y_cut / delta_xy_max

        delta_y_cut = np.diff(y_cut, prepend=y_cut[0])
        delta_y_norm = delta_y_cut / diff_xy_max

        v_norm = v_cut / v_max

        delta_v = np.diff(v_cut, prepend=v_cut[0])
        delta_v_norm = delta_v / diff_v_max

        # Plot the training data
        plot_data = False
        if plot_data:
            if index == 0:
                route = 1
            else:
                route = 3

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, x_norm, linewidth=1)
            plt.grid()
            plt.title("Normalized x Position - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel('x ' r'$[-]$')
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, x_norm.size / 10])
            plt.xticks(np.arange(0, x_norm.size / 10 + 60, 60))
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_x_{}.pdf".format(route)), dpi=900,
                        format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, delta_x_norm, label="delta_x", linewidth=1)
            plt.grid()
            plt.title("Normalized " r'$\Delta$' "x - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel(r'$\Delta$' "x " r'$[-]$')
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, delta_x_norm.size / 10])
            plt.xticks(np.arange(0, x_norm.size / 10 + 60, 60))
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_dx_{}.pdf".format(route)), dpi=900,
                        format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, y_norm, linewidth=1)
            plt.grid()
            plt.title("Normalized y Position - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel("y " r"$[-]$")
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, y_norm.size / 10])
            plt.xticks(np.arange(0, y_norm.size/10+60, 60))
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_y_{}.pdf".format(route)), dpi=900, format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, delta_y_norm, label="delta_y", linewidth=1)
            plt.grid()
            plt.title("Normalized " r'$\Delta$' "y - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel(r'$\Delta$' "y " r'$[-]$')
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, delta_x_norm.size / 10])
            plt.xticks(np.arange(0, y_norm.size/10+60, 60))
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_dy_{}.pdf".format(route)), dpi=900, format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(x_norm, y_norm, linewidth=1)
            plt.grid()
            plt.title("Normalized Path - Route {}".format(route))
            plt.xlabel("x " r"$[-]$")
            plt.ylabel("y " r"$[-]$")
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_path_{}.pdf".format(route)), dpi=900, format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, v_norm, linewidth=1)
            plt.title("Normalized Velocity - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel("v " r"$[-]$")
            plt.ylim(ymin=-0.0625, ymax=1.0625)
            plt.xlim([0, v_norm.size / 10])
            plt.xticks(np.arange(0, v_norm.size/10+60, 60))
            plt.yticks(np.arange(0, 1.1, 0.125))
            plt.grid()
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_v_{}.pdf".format(route)), dpi=900, format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, delta_v_norm, label="delta_v", linewidth=1)
            plt.grid()
            plt.title("Normalized " r'$\Delta$' "v - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel(r'$\Delta$' "v " r'$[-]$')
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, delta_v_norm.size / 10])
            plt.xticks(np.arange(0, v_norm.size/10+60, 60))
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_dv_{}.pdf".format(route)), dpi=900, format='pdf')

        # Combine trajectory signals without standing times into one array
        trajectories = np.transpose(np.array([x_cut, y_cut, v_cut]))
        # List with trajectories for all routes
        trajectories_list.append(trajectories)

        # Combine normalized trajectory signals without standing times into one array
        trajectories_normalized = np.transpose(np.array([delta_x_norm[:-1], delta_y_norm[:-1], delta_v_norm[:-1], delta_x_norm[1:], delta_y_norm[1:], delta_v_norm[1:]]))
        # List with normalized trajectories for all routes
        trajectories_normalized_list.append(trajectories_normalized)

    # Convert to array
    if len(trajectories_list) > 1:
        trajectories_array = np.concatenate((trajectories_list[0], trajectories_list[1]), axis=0)
        trajectories_array_normalized = np.concatenate(
            (trajectories_normalized_list[0], trajectories_normalized_list[1]), axis=0)
    else:
        trajectories_array = np.asarray(trajectories_list)[0]
        trajectories_array_normalized = np.asarray(trajectories_normalized_list)[0]

    # Same for the reference data
    for index, (item, cut_ind) in enumerate(zip(reference_signals, cut_index_list)):
        data = pd.read_csv(item)

        delta = data.delta.to_numpy()
        acceleration = data.a.to_numpy()

        # cut out all indices where the velocity is zero
        delta_cut = delta[cut_ind]
        acceleration_cut = acceleration[cut_ind]
        t_plot = np.arange(0, delta_cut.size / 10, 0.1)

        delta_norm = delta_cut / delta_max

        acceleration_norm = acceleration_cut / a_max

        # Plot the training data
        plot_data = False
        if plot_data:
            if index == 0:
                route = 1
            elif index == 1:
                route = 3

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, delta_norm, linewidth=1)
            plt.title("Normalized Steering Angle - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel(r'$\delta$ ' r"$[-]$")
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, delta_norm.size / 10])
            plt.xticks(np.arange(0, delta_norm.size / 10 + 60, 60))
            plt.grid()
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_delta_{}.pdf".format(route)),
                        dpi=900,
                        format='pdf')

            plt.clf()
            plt.figure(1, figsize=(6, 4))
            plt.plot(t_plot, acceleration_norm, linewidth=1)
            plt.title("Normalized Acceleration - Route {}".format(route))
            plt.xlabel("Time " r"$[s]$")
            plt.ylabel("a " r"$[-]$")
            plt.ylim(ymin=-1.125, ymax=1.125)
            plt.xlim([0, delta_norm.size / 10])
            plt.xticks(np.arange(0, delta_norm.size/10+60, 60))
            plt.grid()
            plt.savefig(os.path.join(path_to_results, "training_filtered_normalized_acc_{}.pdf".format(route)), dpi=900,
                        format='pdf')

        references = np.transpose(np.array([delta_cut, acceleration_cut]))
        references_list.append(references)

        references_normalized = np.transpose(np.array([delta_norm[:-1], acceleration_norm[:-1]]))
        references_normalized_list.append(references_normalized)

    if len(references_list) > 1:
        references_array = np.concatenate((references_list[0], references_list[1]), axis=0)
        references_array_normalized = np.concatenate((references_normalized_list[0], references_normalized_list[1]),
                                                     axis=0)
    else:
        references_array = np.asarray(references_list)[0]
        references_array_normalized = np.asarray(references_normalized_list)[0]

    return trajectories_array, trajectories_array_normalized, references_array, references_array_normalized, max_values


'''# To Test the training_data.py script (please uncomment when called by the main script)
path = "../data"
# Gaimersheim
trajectory1 = os.path.join(path, "waypoints_20180810150607_bus_signals.csv")
reference1 = os.path.join(path, "reference_20180810150607_bus_signals.csv")
# Ingolstadt
trajectory2 = os.path.join(path, "waypoints_20190401145936_bus_signals.csv")
reference2 = os.path.join(path, "reference_20190401145936_bus_signals.csv")
# München
trajectory3 = os.path.join(path, "waypoints_20190401121727_bus_signals.csv")
reference3 = os.path.join(path, "reference_20190401121727_bus_signals.csv")

# training: route 1 & 3
trajectory_training = [trajectory1, trajectory3]
reference_training = [reference1, reference3]

# if not existing: please create the folder "training_data" within the results folder
path_to_results = os.path.join("../results", "training_data")

trajectories_array, trajectories_array_norm, references_array, references_array_norm, max_values \
    = training_data(trajectory_training, reference_training, path_to_results)
'''