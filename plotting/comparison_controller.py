import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np


# Load simulation results of pid & stanley controller and ncp controller
path_to_results_pid_stanley = os.path.join("../results", "PID_Stanley_vehicle_model", "simulation_results.csv")
path_to_results_ncp = os.path.join("../results", "NCP_vehicle_model", "prediction_results.csv")
# Load trajectory and reference data
path_to_trajectory = os.path.join("../results", "PID_Stanley_vehicle_model", "reference_trajectory.csv")
path_to_reference = os.path.join("../results", "PID_Stanley_vehicle_model", "target.csv")
# Path to save the plots
# if not existing: please create the folder "plots_pid_stanley_ncp_comparison" within the results folder
path_to_results = "plots_pid_stanley_ncp_comparison"

# Read in the data
results_1 = pd.read_csv(path_to_results_ncp)
results_2 = pd.read_csv(path_to_results_pid_stanley)
trajectory = pd.read_csv(path_to_trajectory)
reference = pd.read_csv(path_to_reference)


def steering_angle_comparison(results_1, results_2, reference):
    x_max = len(results_1.delta_history)
    t_plot = np.arange(0, x_max / 10, 0.1)

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(4, figsize=(6, 4))
    plt.plot(t_plot, reference.delta[:-2] / 15.8, label='Recorded')
    plt.plot(t_plot, results_2.delta_history[:-2], label='Calculated')
    plt.plot(t_plot, results_1.delta_history, label='Predicted')
    plt.title('Steering Angle')
    plt.legend()
    plt.grid(True)
    plt.xlim([0, x_max / 10])
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel("Steering Angle " r'$[rad]$')
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "delta_compare.pdf"), dpi=900,
                format='pdf')


def acceleration_comparison(results_1, results_2, reference):
    x_max = len(results_1.a_history)
    t_plot = np.arange(0, x_max / 10, 0.1)

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(4, figsize=(6, 4))
    plt.plot(t_plot, reference.a[:-2], label='Recorded')
    plt.plot(t_plot, results_2.a_history[:-2], label='Calculated')
    plt.plot(t_plot, results_1.a_history, label='Predicted')
    plt.title('Acceleration')
    plt.legend()
    plt.grid(True)
    plt.xlim([0, x_max / 10])
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel("Acceleration " r'$[m/s^2]$')
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "a_compare.pdf"), dpi=900,
                format='pdf')


steering_angle_comparison(results_1, results_2, reference)

acceleration_comparison(results_1, results_2, reference)
