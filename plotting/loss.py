import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd


# Path to log data
path_to_logs = os.path.join("../results", "NCP_training")
# Path to save the plots
# if not existing: please create the folder "plots_loss" within the results folder
path_to_results = "plots_loss"

# Parameter of model whose loss is to be plotted
numb_neurons_list = [32]
lr_list = [0.01]
grad_clip_list = [1.0]

test_losses = []
test_loss_index = []

for numb_neurons in numb_neurons_list:
    for lr in lr_list:
        for grad_clip in grad_clip_list:
            path_to_loss = os.path.join(path_to_logs, "log_{}_{}_{}".format(numb_neurons, lr, grad_clip),
                                        "lightning_logs", "version_0", "metrics.csv")
            metrics = pd.read_csv(path_to_loss)
            train_loss = metrics.train_loss
            train_loss = train_loss.dropna()
            train_loss_array = train_loss.to_numpy()
            val_loss = metrics.val_loss
            val_loss = val_loss.dropna()
            val_loss_array = val_loss.to_numpy()
            test_loss = val_loss_array[-1]
            test_loss_index.append("log_{}_{}_{}".format(numb_neurons, lr, grad_clip))
            test_losses.append(test_loss)
            val_loss = val_loss_array[:-1]

            # Printing training loss
            epochs = np.arange(0, 500, 1)
            plt.clf()
            plt.rcParams['font.size'] = 10
            plt.figure(1, figsize=(12, 4))
            plt.plot(epochs, train_loss, label="Loss")
            plt.grid()
            plt.xlim((0, 499))
            plt.ylabel("Loss " r"$[-]$")
            plt.xlabel("Epochs " r"$[-]$")
            plt.title("Training Loss")
            plt.tight_layout()
            plt.savefig(os.path.join(path_to_results, "training_loss_log_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)),dpi=900, format='pdf')

            # Printing validation loss
            epochs = np.arange(0, 500, 1)
            plt.clf()
            plt.rcParams['font.size'] = 10
            plt.figure(1, figsize=(12, 4))
            plt.plot(epochs, val_loss, label="Loss")
            plt.grid()
            plt.xlim((0, 499))
            plt.ylabel("Loss " r"$[-]$")
            plt.xlabel("Epochs " r"$[-]$")
            plt.title("Validation Loss")
            plt.tight_layout()
            plt.savefig(os.path.join(path_to_results, "validation_loss_log_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf')

# save test loss to excel
test_loss_array = np.transpose(np.asarray([test_loss_index, test_losses]))
pd.DataFrame(test_loss_array).to_csv(os.path.join(path_to_results, "test_loss.csv"), header=["log", "test_loss"])
