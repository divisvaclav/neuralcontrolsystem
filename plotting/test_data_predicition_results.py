import numpy as np
import torch.nn as nn
from ncps.wirings import AutoNCP
from ncps.torch import LTC
import pytorch_lightning as pl
import torch
import torch.utils.data as data
import matplotlib.pyplot as plt
import os

from libs import training_data


# Path to log data
path_to_logs = os.path.join("../results", "NCP_training")
# Path to save the plots
# if not existing: please create the folder "plots_test_data_prediction" within the results folder
path_to_results = "plots_test_data_prediction"

# Load trajectory and reference data
# Gaimersheim
trajectory1 = os.path.join("../data", "waypoints_20180810150607_bus_signals.csv")
reference1 = os.path.join("../data", "reference_20180810150607_bus_signals.csv")
# Ingolstadt
trajectory2 = os.path.join("../data", "waypoints_20190401145936_bus_signals.csv")
reference2 = os.path.join("../data", "reference_20190401145936_bus_signals.csv")
# München
trajectory3 = os.path.join("../data", "waypoints_20190401121727_bus_signals.csv")
reference3 = os.path.join("../data", "reference_20190401121727_bus_signals.csv")

# Training: route 1, 2 & 3
trajectory_training = [trajectory1, trajectory2, trajectory3]
reference_training = [reference1, reference2, reference3]

trajectories, trajectories_normalized, references, references_normalized, max_values = \
    training_data.training_data(trajectory_training, reference_training, path_to_results)

data_x = torch.from_numpy(np.expand_dims(trajectories_normalized[:1000], axis=0)).double()
data_y = torch.from_numpy(np.expand_dims(references_normalized[:1000], axis=0)).double()
val_x = torch.from_numpy(np.expand_dims(trajectories_normalized[9000:10000], axis=0)).double()
val_y = torch.from_numpy(np.expand_dims(references_normalized[9000:10000], axis=0)).double()
test_x = torch.from_numpy(np.expand_dims(trajectories_normalized[10000:], axis=0)).double()
test_y = torch.from_numpy(np.expand_dims(references_normalized[10000:], axis=0)).double()


# Plot prediction of the ncp model on the test data
def plot_prediction(test_y, prediction, numb_neurons, lr, grad_clip):
    t = np.arange(0, 1318 / 10, 0.1)
    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(figsize=(6, 4))
    plt.plot(t, test_y[0, :, 0], label="Target")
    plt.plot(t, prediction[0, :, 0], label="Prediction")
    plt.grid()
    plt.xlim((0, 132))
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel('Steering angle ' r"$[-]$")
    plt.title("Steering Angle")
    plt.legend(loc="lower right")
    plt.tight_layout()
    plt.savefig(
        os.path.join(path_to_results, "prediction_delta_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)),
        dpi=900, format='pdf')

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(figsize=(6, 4))
    plt.plot(t, test_y[0, :, 1], label="Target")
    plt.plot(t, prediction[0, :, 1], label="Prediction")
    plt.grid()
    plt.xlim((0, 132))
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel('Acceleration ' r"$[-]$")
    plt.title("Acceleration")
    plt.legend(loc="upper right")
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "prediction_a_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)),
                dpi=900, format='pdf')


# Plot the steering angle and the error between prediction and recorded steering angle as a subplot
def plot_delta_error(test_y, prediction, numb_neurons, lr, grad_clip):
    error_delta = np.abs(test_y[0, :, 0]) - np.abs(prediction[0, :, 0])

    t = np.arange(0, 1318 / 10, 0.1)
    plt.rcParams['font.size'] = 10
    plt.figure(1, figsize=(12, 4))
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    fig.suptitle('NCP prediction after training', x=0.56)
    fig.tight_layout(pad=2.0)
    fig.subplots_adjust(top=0.6)
    ax1.plot(t, test_y[0, :, 0], label="Target")
    ax1.plot(t, prediction[0, :, 0], label="Prediction")
    ax2.plot(t, error_delta)
    ax1.grid()
    ax2.grid()
    ax1.set_xlim([0, 132])
    ax2.set_xlim([0, 132])
    ax1.tick_params(axis="y")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.set_xlabel("Time [s]")
    ax1.set_ylabel("Steering angle " r'$[-]$')
    ax2.set_ylabel("Error "r'$[-]$')
    ax1.legend(loc="upper right", ncol=1)
    plt.tight_layout()
    fig.savefig(os.path.join(path_to_results, "prediction_error_delta_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf', bbox_inches='tight')


# Plot a section of the steering angle and the error between prediction and recorded steering angle as a subplot
def plot_section_delta_error(test_y, prediction, numb_neurons, lr, grad_clip):
    error_delta = np.abs(test_y[0, :, 0]) - np.abs(prediction[0, :, 0])

    t = np.arange(0, 250 / 10, 0.1)
    plt.rcParams['font.size'] = 10
    plt.figure(1, figsize=(12, 4))
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    fig.suptitle('NCP prediction after training', x=0.56)
    fig.tight_layout(pad=2.0)
    fig.subplots_adjust(top=0.6)
    ax1.plot(t, test_y[0, :, 0][:250], label="Target")
    ax1.plot(t, prediction[0, :, 0][:250], label="Prediction")
    ax2.plot(t, error_delta[:250])
    ax1.grid()
    ax2.grid()
    ax1.set_xlim([0, 25])
    ax2.set_xlim([0, 25])
    ax1.tick_params(axis="y")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.set_xlabel("Time [s]")
    ax1.set_ylabel("Steering angle " r'$[-]$')
    ax2.set_ylabel("Error "r'$[-]$')
    ax1.legend(loc="upper right", ncol=1)
    plt.tight_layout()
    fig.savefig(os.path.join(path_to_results, "section_prediction_error_delta_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf', bbox_inches='tight')


# Plot the acceleration and the error between prediction and recorded acceleration as a subplot
def plot_a_error(test_y, prediction, numb_neurons, lr, grad_clip):
    error_a = np.abs(test_y[0, :, 1]) - np.abs(prediction[0, :, 1])

    t = np.arange(0, 1318 / 10, 0.1)
    plt.rcParams['font.size'] = 10
    plt.figure(1, figsize=(12, 4))
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    fig.suptitle('NCP prediction after training', x=0.56)
    fig.tight_layout(pad=2.0)
    fig.subplots_adjust(top=0.6)
    ax1.plot(t, test_y[0, :, 1], label="Target")
    ax1.plot(t, prediction[0, :, 1], label="Prediction")
    ax2.plot(t, error_a)
    ax1.grid()
    ax2.grid()
    ax1.set_xlim([0, 132])
    ax2.set_xlim([0, 132])
    ax1.tick_params(axis="y")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.set_xlabel("Time [s]")
    ax1.set_ylabel("Steering angle " r'$[-]$')
    ax2.set_ylabel("Error "r'$[-]$')
    ax1.legend(loc="upper right", ncol=1)
    plt.tight_layout()
    fig.savefig(os.path.join(path_to_results, "prediction_error_a_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf', bbox_inches='tight')


# Plot a section of the acceleration and the error between prediction and recorded acceleration as a subplot
def plot_section_a_error(test_y, prediction, numb_neurons, lr, grad_clip):
    error_a = np.abs(test_y[0, :, 1]) - np.abs(prediction[0, :, 1])

    t = np.arange(0, 250 / 10, 0.1)
    plt.rcParams['font.size'] = 10
    plt.figure(1, figsize=(12, 4))
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    fig.suptitle('Acceleration', x=0.56)
    fig.tight_layout(pad=2.0)
    fig.subplots_adjust(top=0.6)
    ax1.plot(t, test_y[0, :, 1][:250], label="Target")
    ax1.plot(t, prediction[0, :, 1][:250], label="Prediction")
    ax2.plot(t, error_a[:250])
    ax1.grid()
    ax2.grid()
    ax1.set_xlim([0, 25])
    ax2.set_xlim([0, 25])
    ax1.tick_params(axis="y")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.set_xlabel("Time [s]")
    ax1.set_ylabel("Acceleration " r'$[-]$')
    ax2.set_ylabel("Error "r'$[-]$')
    ax1.legend(loc="upper right", ncol=1)
    plt.tight_layout()
    fig.savefig(os.path.join(path_to_results, "section_prediction_error_a_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)), dpi=900, format='pdf', bbox_inches='tight')


# Load the trained model, get the predictions and create the plots
numb_neurons_list = [8]
lr_list = [0.01]
grad_clip_list = [1.0]

for numb_neurons in numb_neurons_list:
    for lr in lr_list:
        for grad_clip in grad_clip_list:
            # Path to model parameters; last entry might have to be adapted depending on the number of epochs
            path_to_model = os.path.join(path_to_logs, "log_{}_{}_{}".format(numb_neurons, lr, grad_clip),
                                         "lightning_logs", "version_0", "checkpoints", "epoch=2-step=3.ckpt")

            # Initialize model
            in_features = 6
            out_features = 2
            wiring = AutoNCP(numb_neurons, out_features)
            ltc_model = LTC(in_features, wiring, batch_first=True)
            # Load state dict
            checkpoint = torch.load(path_to_model)
            state_dict = torch.load(path_to_model)["state_dict"]
            # create new OrderedDict that does not contain `module.`
            from collections import OrderedDict
            new_state_dict = OrderedDict()
            for k, v in state_dict.items():
                name = k[6:]  # remove `module.`
                new_state_dict[name] = v

            # Load params
            ltc_model.load_state_dict(new_state_dict)

            # Predictions on test data
            with torch.no_grad():
                prediction = ltc_model(test_x)[0].numpy()

            # Different plots
            plot_prediction(test_y, prediction, numb_neurons, lr, grad_clip)

            plot_delta_error(test_y, prediction,numb_neurons, lr, grad_clip)

            plot_section_delta_error(test_y, prediction,numb_neurons, lr, grad_clip)

            plot_a_error(test_y, prediction,numb_neurons, lr, grad_clip)

            plot_section_a_error(test_y, prediction, numb_neurons, lr, grad_clip)



