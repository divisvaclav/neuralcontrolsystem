import pandas as pd
import matplotlib.pyplot as plt
import os

# Path to log folder
path_to_logs = os.path.join("../results", "NCP_reproducibility")
path_to_results = path_to_logs

numb_neurons, lr, grad_clip = 32, 0.01, 1.0  # have to be the same parameters as in the log file

# Initialize lists to save the loss
test_losses = []
test_loss_index = []
train_loss_list = []
val_loss_list = []

# Loop through number of training repetitions, here n=30
for i in range(30):
    # Path to loss file
    path_to_loss = os.path.join(path_to_logs, "log_{}_{}_{}_{}".format(numb_neurons, lr, grad_clip, i), "lightning_logs", "version_0", "metrics.csv")
    # Open loss file
    metrics = pd.read_csv(path_to_loss)
    # Read in training loss
    train_loss = metrics.train_loss
    train_loss = train_loss.dropna()
    train_loss_array = train_loss.to_numpy()
    train_loss_list.append(train_loss_array[-1])
    # Read in validation loss
    val_loss = metrics.val_loss
    val_loss = val_loss.dropna()
    val_loss_array = val_loss.to_numpy()
    # Read in test loss
    test_loss = val_loss_array[-1]
    test_loss_index.append("log_{}_{}_{}_{}".format(numb_neurons, lr, grad_clip, i))
    test_losses.append(test_loss)

    val_loss = val_loss_array[:-1]
    val_loss_list.append(val_loss[-1])

# Create box plot
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(6, 4))
plt.boxplot([train_loss_list, val_loss_list, test_losses], patch_artist=True, labels=['training', 'validation', 'test'])
plt.title("Reproducibility of training, validation and test")
plt.ylabel("Loss [-]")
plt.grid(True, linestyle='-', which='major', axis='y', color='lightgrey', alpha=0.5)
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "box_plot.pdf"), dpi=900, format='pdf')
