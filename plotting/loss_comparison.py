import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

# Path to log data
path_to_logs = os.path.join("../results", "NCP_training")
# Path to save the plots
# if not existing: please create the folder "plots_loss" within the results folder
path_to_results = "plots_loss"

train_loss_list = []
val_loss_list = []
test_losses = []
test_loss_index = []

models = ["log_64_0.05_10.0", "log_32_0.01_1.0", "log_64_0.05_0.1"]
for index, item in enumerate(models):
    path_to_loss = os.path.join(path_to_logs, item, "lightning_logs", "version_0", "metrics.csv")
    metrics = pd.read_csv(path_to_loss)
    train_loss = metrics.train_loss
    train_loss = train_loss.dropna()
    train_loss_array = train_loss.to_numpy()
    train_loss_list.append(train_loss_array)

    val_loss = metrics.val_loss
    val_loss = val_loss.dropna()
    val_loss_array = val_loss.to_numpy()

    val_loss = val_loss_array[:-1]
    val_loss_list.append(val_loss)

loss_array = np.concatenate((train_loss_list[0], train_loss_list[1], train_loss_list[2]), axis=0)
validation_loss_array = np.concatenate((val_loss_list[0], val_loss_list[1], val_loss_list[2]), axis=0)


# Printing training loss
epochs = np.arange(0, 500, 1)
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
plt.plot(epochs, loss_array[0:500], label="Best")
plt.plot(epochs, loss_array[500:1000], label="Inital")
plt.plot(epochs, loss_array[1000:], label="Worst")
plt.grid()
plt.xlim((0, 499))
plt.ylabel("Loss " r"$[-]$")
plt.xlabel("Epochs " r"$[-]$")
plt.legend()
plt.title("Training Loss")
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "training_loss_3.pdf"), dpi=900, format='pdf')

# Printing validation loss
epochs = np.arange(0, 500, 1)
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
plt.plot(epochs, validation_loss_array[0:500], label="Best")
plt.plot(epochs, validation_loss_array[500:1000], label="Initial")
plt.plot(epochs, validation_loss_array[1000:], label="Worst")
plt.grid()
plt.xlim((0, 499))
plt.ylabel("Loss " r"$[-]$")
plt.xlabel("Epochs " r"$[-]$")
plt.title("Validation Loss")
plt.legend()
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "validation_loss_3.pdf"), dpi=900, format='pdf')

# Zoom in plot for training loss
l1 = loss_array[0:500]  # y-axis
l2 = loss_array[500:1000]  # y-axis
l3 = loss_array[1000:]  # y-axis

# Plot
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
ax = plt.axes()
plt.plot(epochs, l1, label="Best")
plt.plot(epochs, l2, label="Inital")
plt.plot(epochs, l3, label="Worst")
plt.legend(loc='upper left')
plt.xlim((0, 499))
plt.grid()
plt.ylabel("Loss " r"$[-]$")
plt.xlabel("Epochs " r"$[-]$")
plt.title("Training Loss")
plt.tight_layout()

# Select the x-range for the zoomed region
x1 = 429
x2 = 499

# Select y-range for zoomed region
y1 = 0.001
y2 = 0.019

# Make the zoom-in plot:
axins = zoomed_inset_axes(ax, 3, loc=1) # zoom = 2
axins.plot(l1)
axins.plot(l2)
axins.plot(l3)
axins.set_xlim(x1, x2)
axins.set_ylim(y1, y2)
plt.xticks(visible=False)
plt.yticks(visible=True)
mark_inset(ax, axins, loc1=3, loc2=4, fc="none", ec="0.5")
plt.savefig(os.path.join(path_to_results, "training_loss_3_zoom.pdf"), dpi=900, format='pdf')

# Zoom in plot for validation loss
v1 = validation_loss_array[0:500]
v2 = validation_loss_array[500:1000]
v3 = validation_loss_array[1000:]

# Plot
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(1, figsize=(12, 4))
ax = plt.axes()
plt.plot(epochs, v1, label="Best")
plt.plot(epochs, v2, label="Inital")
plt.plot(epochs, v3, label="Worst")
plt.legend(loc='upper left')
plt.xlim((0, 499))
plt.grid()
plt.ylabel("Loss " r"$[-]$")
plt.xlabel("Epochs " r"$[-]$")
plt.title("Validation Loss")
plt.tight_layout()

# Select the x-range for the zoomed region
x1 = 429
x2 = 499

# Select y-range for zoomed region
y1 = 0.003
y2 = 0.025

# Make the zoom-in plot:
axins = zoomed_inset_axes(ax, 3, loc=1) # zoom = 2
axins.plot(v1)
axins.plot(v2)
axins.plot(v3)
axins.set_xlim(x1, x2)
axins.set_ylim(y1, y2)
plt.xticks(visible=False)
plt.yticks(visible=True)
mark_inset(ax, axins, loc1=3, loc2=4, fc="none", ec="0.5")
plt.savefig(os.path.join(path_to_results, "validation_loss_3_zoom.pdf"), dpi=900, format='pdf')