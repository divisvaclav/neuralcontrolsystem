import numpy as np
from ncps.wirings import AutoNCP
from ncps.torch import LTC
import torch
import matplotlib.pyplot as plt
import os
from libs import training_data
import tensorflow as tf


# Path to the log folder
path_to_logs = os.path.join("../results", "NCP_training")
# Path to save the plots
# if not existing: please create the folder "steering_angle_plot" within the results folder
path_to_results = "plot_steering_angle_comparison"

# Load trajectory and reference data
# Gaimersheim
trajectory1 = os.path.join("data", "waypoints_20180810150607_bus_signals.csv")
reference1 = os.path.join("data", "reference_20180810150607_bus_signals.csv")
# Ingolstadt
trajectory2 = os.path.join("data", "waypoints_20190401145936_bus_signals.csv")
reference2 = os.path.join("data", "reference_20190401145936_bus_signals.csv")
# München
trajectory3 = os.path.join("data", "waypoints_20190401121727_bus_signals.csv")
reference3 = os.path.join("data", "reference_20190401121727_bus_signals.csv")

# Training: route 1,2 & 3 -> later split into training, test and validation sets with 80/10/10
trajectory_training = [trajectory1, trajectory2, trajectory3]
reference_training = [reference1, reference2, reference3]

trajectories, trajectories_normalized, references, references_normalized, max_values = \
    training_data.training_data(trajectory_training, reference_training, path_to_results)

data_x = torch.from_numpy(np.expand_dims(trajectories_normalized[:1000], axis=0)).double()
data_y = torch.from_numpy(np.expand_dims(references_normalized[:1000], axis=0)).double()
val_x = torch.from_numpy(np.expand_dims(trajectories_normalized[9000:10000], axis=0)).double()
val_y = torch.from_numpy(np.expand_dims(references_normalized[9000:10000], axis=0)).double()
test_x = torch.from_numpy(np.expand_dims(trajectories_normalized[10000:], axis=0)).double()
test_y = torch.from_numpy(np.expand_dims(references_normalized[10000:], axis=0)).double()

# Define trained model with different parameters which should be compared
models = ["log_64_0.05_10.0", "log_32_0.01_1.0", "log_64_0.05_0.1"]

prediction_list = []

for index, item in enumerate(models):
    # Path to model parameters; last entry might have to be adapted depending on the number of epochs
    path_to_model = os.path.join(path_to_logs, item, "lightning_logs", "version_0", "checkpoints", "epoch=499-step=500.ckpt")

    # Initialize model
    in_features = 6
    out_features = 2
    if index == 1:
        numb_neurons = 32
    else:
        numb_neurons = 64

    wiring = AutoNCP(numb_neurons, out_features)
    ltc_model = LTC(in_features, wiring, batch_first=True)

    # Load state dict
    checkpoint = torch.load(path_to_model)
    state_dict = torch.load(path_to_model)["state_dict"]
    # Create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[6:]  # remove `module.`
        new_state_dict[name] = v

    # Load params
    ltc_model.load_state_dict(new_state_dict)

    with torch.no_grad():
        prediction = ltc_model(test_x)[0].numpy()
        prediction = tf.squeeze(prediction)
        prediction = prediction.numpy()
        prediction_list.append((prediction[:, 0]))

# Generate array with prediction results (here: 3 models to compare -> input of concatenate has to be change if another number of models is compared)
prediction_array = np.concatenate((prediction_list[0], prediction_list[1], prediction_list[2]), axis=0)

# Plot the steering angle of the compared models
t = np.arange(0, 1318 / 10, 0.1)
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(figsize=(6, 4))
plt.plot(t, test_y[0, :, 0], 'k', label="Target")
plt.plot(t, prediction_array[0:1318], label="Best")
plt.plot(t, prediction_array[1318:2636], label="Initial")
plt.plot(t, prediction_array[2636:], label="Worst")
plt.grid()
plt.xlim((0, 132))
plt.xlabel("Time " r"$[s]$")
plt.ylabel('Steering Angle ' r"$[rad]$")
plt.title("Steering Angle")
plt.legend(loc="lower right")
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "steering_angles.pdf"), dpi=900, format='pdf')
