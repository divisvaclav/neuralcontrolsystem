import numpy as np
from ncps.wirings import AutoNCP
from ncps.torch import LTC
import torch
import torch.utils.data as data
import matplotlib.pyplot as plt
import seaborn as sns
import os

from libs import training_data


# Path to save the plots
path_to_results = "plots_test_data"

# Load trajectory and reference data
# Gaimersheim
trajectory1 = os.path.join("../data", "waypoints_20180810150607_bus_signals.csv")
reference1 = os.path.join("../data", "reference_20180810150607_bus_signals.csv")
# Ingolstadt
trajectory2 = os.path.join("../data", "waypoints_20190401145936_bus_signals.csv")
reference2 = os.path.join("../data", "reference_20190401145936_bus_signals.csv")
# München
trajectory3 = os.path.join("../data", "waypoints_20190401121727_bus_signals.csv")
reference3 = os.path.join("../data", "reference_20190401121727_bus_signals.csv")

# Training: route 1, 2 & 3
trajectory_training = [trajectory1, trajectory2, trajectory3]
reference_training = [reference1, reference2, reference3]

trajectories, trajectories_normalized, references, references_normalized, max_values = \
    training_data.training_data(trajectory_training, reference_training, path_to_results)

# Data for the simulation of the PID and Stanley controller
test_x = torch.from_numpy(np.expand_dims(trajectories[10000:], axis=0)).double()
test_y = torch.from_numpy(np.expand_dims(references[10000:], axis=0)).double()

# Visualize a section of the test data for PID and Stanley controller
t = np.arange(0, 250 / 10, 0.1)
plt.rcParams['font.size'] = 10
plt.figure(figsize=(6, 4))
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
fig.suptitle('Test Data')
fig.tight_layout(pad=1.0)
fig.subplots_adjust(top=0.9)
x = ax1.plot(t, test_x[0, :, 0][0:250], "#1f77b4", label="x")
ax2.plot(t, test_x[0, :, 2][0:250], "#2ca02c", label="Velocity")
ax1.grid()
ax2.grid()
ax3 = ax1.twinx()
y = ax3.plot(t, test_x[0, :, 1][0:250], '#ff7f0e',  label="y")
ax1.set_xlim([0, 25])
ax2.set_xlim([0, 25])
ax1.tick_params(axis="y")
ax2.tick_params(axis="x")
ax2.tick_params(axis="y")
ax2.set_xlabel("Time [s]")
ax1.set_ylabel("x Position [m]")
ax3.set_ylabel("y Position [m]")
ax2.set_ylabel("Velocity "r'$[m/s]$')
lines = x+y
labs = [l.get_label() for l in lines]
ax1.legend(lines, labs, loc="upper right")
fig.savefig(os.path.join(path_to_results, "test_data_mc.pdf"), dpi=900, format='pdf', bbox_inches='tight')

# Training data for the ncp model
data_x_norm = torch.from_numpy(np.expand_dims(trajectories_normalized[:9000], axis=0)).double()
data_y_norm = torch.from_numpy(np.expand_dims(references_normalized[:9000], axis=0)).double()

trainloader = data.DataLoader(
    data.TensorDataset(data_x_norm, data_y_norm), batch_size=1, shuffle=False, num_workers=4
)

# Visualize a section of the training data
t = np.arange(0, 250 / 10, 0.1)
plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(figsize=(6, 4))
plt.plot(t, data_x_norm[0, :, 0][:250], label=r'$\Delta x(t)$')
plt.plot(t, data_x_norm[0, :, 3][:250], label=r'$\Delta x(t+1)$')
plt.plot(t, data_x_norm[0, :, 1][:250], label=r'$\Delta y(t)$')
plt.plot(t, data_x_norm[0, :, 4][:250], label=r'$\Delta y(t+1)$')
plt.plot(t, data_x_norm[0, :, 2][:250], label=r'$\Delta v(t)$')
plt.plot(t, data_x_norm[0, :, 5][:250], label=r'$\Delta v(t+1)$')
plt.grid()
plt.ylim((-1, 1))
plt.xlim([0, 25])
plt.ylabel("y " r"$[-]$")
plt.xlabel("Time [s]")
plt.title("Training Data - Input")
plt.legend(loc='best',ncol=3)
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "training_data_input.pdf"), dpi=900, format='pdf', bbox_inches='tight')

plt.clf()
plt.rcParams['font.size'] = 10
plt.figure(figsize=(6, 4))
plt.plot(t, data_y_norm[0, :, 0][:250], label=r'$\delta$')
plt.plot(t, data_y_norm[0, :, 1][:250], label=r'$a$')
plt.grid()
plt.ylim((-1, 1))
plt.xlim([0, 25])
plt.ylabel("y " r"$[-]$")
plt.xlabel("Time [s]")
plt.title("Training Data - Target")
plt.legend(loc='best', ncol=3)
plt.tight_layout()
plt.savefig(os.path.join(path_to_results, "training_data_target.pdf"), dpi=900, format='pdf', bbox_inches='tight')

# Visualize the ncp network
def plot_network(wiring, numb_neurons):
    sns.set_style("white")
    plt.figure(figsize=(12, 12))
    plt.rcParams['font.size'] = 16
    legend_handles = wiring.draw_graph(draw_labels=False,  neuron_colors={"command": "tab:cyan"})
    plt.legend(handles=legend_handles, loc="lower center", ncol=2)
    sns.despine(left=True, bottom=True)
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "ncp_network_{}.pdf".format(numb_neurons)), dpi=900, format='pdf', bbox_inches='tight')


in_features = 6
out_features = 2
wiring = AutoNCP(32, out_features)  # 32 units, 1 motor neuron
ltc_model = LTC(in_features, wiring, batch_first=True, return_sequences=True) # wiring
plot_network(wiring, 32)
