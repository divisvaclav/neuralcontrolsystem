import os
import matplotlib.pylab as plt
import numpy as np
from libs import pid_controller
from libs import vehicle_model
from libs import stanley_controller
from libs import training_data
import pandas as pd


show_animation = False
first_time = True

# Live plot can be activated with show_animation
def plot_vehicle(x_traj, y_traj, frame):
    plt.plot(x_traj, y_traj, 'bo')
    # for stopping simulation with the esc key.
    plt.gcf().canvas.mpl_connect('key_release_event',
                                 lambda event: [exit(0) if event.key == 'escape' else None])
    plt.title('frame={}'.format(frame))
    plt.pause(0.01)


def plot_history(x_history, y_history, v_history, ve_history, a_history, delta_history, used_index, pid_indexs, trajectory, reference):
    x_max = len(x_history)
    t_plot = np.arange(0, x_max / 10, 0.1)

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(1, figsize=(12, 4))
    plt.plot(trajectory[:, 0], trajectory[:, 1], label='Target')
    plt.plot(x_history, y_history, label='Calculated')
    plt.title('Path')
    plt.grid()
    plt.legend()
    plt.xlabel("x " r"$[m]$")
    plt.ylabel("y " r"$[m]$")
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "path_pid_stanley.pdf"), dpi=900,
                format='pdf')

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(2, figsize=(6, 4))
    plt.plot(t_plot, trajectory[:, 2], label='Target')
    plt.plot(t_plot, v_history, label='Calculated')
    plt.title('Velocity')
    plt.legend()
    plt.grid(True)
    plt.xlim([0, x_max / 10])
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel("Velocity " r'$[m/s]$')
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "v_pid_stanley.pdf"), dpi=900,
                format='pdf')

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(3, figsize=(6, 4))
    plt.plot(t_plot, reference[:, 1], label='Target')
    plt.plot(t_plot, a_history, label='Calculated')
    plt.title('Acceleration')
    plt.legend()
    plt.grid(True)
    plt.xlim([0, x_max / 10])
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel("Acceleration " r'$[m/s^2]$')
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "a_pid_stanley.pdf"), dpi=900,
                format='pdf')

    plt.clf()
    plt.rcParams['font.size'] = 10
    plt.figure(4, figsize=(6, 4))
    plt.plot(t_plot, reference[:, 0] / 15.8, label='Target')
    plt.plot(t_plot, delta_history, label='Calculated')
    plt.title('Steering Angle')
    plt.legend()
    plt.grid(True)
    plt.xlim([0, x_max / 10])
    plt.xlabel("Time " r"$[s]$")
    plt.ylabel("Steering Angle " r'$[rad]$')
    plt.tight_layout()
    plt.savefig(os.path.join(path_to_results, "delta_pid_stanley.pdf"), dpi=900,
                format='pdf')


if __name__ == '__main__':
    # Path to results folder
    # if not existing: please create the folder "PID_Stanley_vehicle_model" within the results folder
    path_to_results = os.path.join("results", "PID_Stanley_vehicle_model")

    # Load trajectory and reference data
    # Gaimersheim
    trajectory1 = os.path.join("data", "waypoints_20180810150607_bus_signals.csv")
    reference1 = os.path.join("data", "reference_20180810150607_bus_signals.csv")
    # Ingolstadt
    trajectory2 = os.path.join("data", "waypoints_20190401145936_bus_signals.csv")
    reference2 = os.path.join("data", "reference_20190401145936_bus_signals.csv")
    # München
    trajectory3 = os.path.join("data", "waypoints_20190401121727_bus_signals.csv")
    reference3 = os.path.join("data", "reference_20190401121727_bus_signals.csv")

    trajectory_training = [trajectory1, trajectory2, trajectory3]
    reference_training = [reference1, reference2, reference3]

    # Here: use the same data as for the testing of the NCP model but the not normalized version
    trajectories, _, references, _, _ = training_data.training_data(trajectory_training, reference_training,
                                                                    path_to_results)

    data_x = trajectories[:9000]
    data_y = references[:9000]
    val_x = trajectories[9000:10000]
    val_y = references[9000:10000]
    test_x = trajectories[10000:]
    test_y = references[10000:]

    trajectory = test_x
    reference = test_y

    # Distance between two successive waypoints
    waypoint_dist = []
    for t in range(1, trajectory.shape[0]):
        waypoint_dist.append(np.sqrt((trajectory[t, 0] - trajectory[t-1, 0]) ** 2 + (trajectory[t, 1] - trajectory[t-1, 1]) ** 2))
    waypoint_dist.append(0)

    # Initial vehicle position, steering angle and acceleration
    x0 = np.array([val_x[-1, 0], val_x[-1, 1], np.radians(0.0), val_x[-1, 2]], dtype=float)
    delta0 = val_y[-1, 0]/15.8
    a0 = val_y[-1, 1]
    state = x0

    # Initialize arrays to save the values of the simulation
    x_history = []
    y_history = []
    yaw_history = []
    v_history = []
    a_history = []
    delta_history = []
    ve_history = []
    used_index = []
    pid_indexs = []

    # Index of waypoint that is currently closest to the car, assumed to be the first index
    closest_index = 0
    steps = 0
    pid_index = 0

    while True:
        steps += 1

        # Update current position
        current_x, current_y, current_yaw, current_v = state[0], state[1], state[2], state[3]

        # Store history
        x_history.append(current_x)
        y_history.append(current_y)
        yaw_history.append(current_yaw)
        v_history.append(current_v)

        # Search for closest distance to the reference path in both directions to get the index of the closest distance (needed by Stanley controller)
        closest_dist = np.linalg.norm(np.array([trajectory[closest_index, 0] - current_x, trajectory[closest_index, 1] - current_y]))
        closest_index_search = closest_index

        index_search = 1
        closest_dist_list = np.array([closest_dist])
        closest_index_list = np.array([closest_index])
        while index_search < 10:
            closest_index_search += 1
            if closest_index_search == trajectory.shape[0]:
                break
            closest_dist = np.linalg.norm(
                np.array([trajectory[closest_index_search, 0] - current_x, trajectory[closest_index_search, 1] - current_y]))
            closest_dist_list = np.append(closest_dist_list, closest_dist)
            closest_index_list = np.append(closest_index_list, closest_index_search)
            index_search += 1

        closest_index_search = closest_index

        index_search = 1
        while index_search < 10:
            closest_index_search -= 1
            if closest_index_search < 0:
                break
            closest_dist = np.linalg.norm(
                np.array([trajectory[closest_index_search, 0] - current_x, trajectory[closest_index_search, 1] - current_y]))
            closest_dist_list = np.append(closest_dist_list, closest_dist)
            closest_index_list = np.append(closest_index_list, closest_index_search)
            index_search += 1

        closest_dist = min(closest_dist_list)
        closest_index = closest_index_list[np.argmin(closest_dist_list)]

        # Return path with 1 waypoint behind the closest index and X waypoints ahead
        # X is the index that has a lookahead distance specified
        waypoint_behind_index = closest_index - 1
        if waypoint_behind_index < 0:
            waypoint_behind_index = 0

        waypoint_ahead_index = closest_index
        dist_ahead = 0.0
        while dist_ahead < 5.0:  # 5 m is the lookahead distance
            dist_ahead += waypoint_dist[waypoint_ahead_index]
            waypoint_ahead_index += 1
            if waypoint_ahead_index >= trajectory.shape[0]:
                waypoint_ahead_index = trajectory.shape[0] - 1.0
                break

        selected_waypoints = trajectory[waypoint_behind_index:waypoint_ahead_index, :]

        closest_waypoint = trajectory[closest_index]
        used_index.append(closest_index)

        # Forward current time stamp to PID controller
        controller_long, e = pid_controller.PID(trajectory[pid_index], state)
        pid_index += 1
        pid_indexs.append(pid_index)

        # Forward list with selected waypoints to Stanley controller
        controller_lat = stanley_controller.stanley(selected_waypoints, state)

        # save controller outputs
        delta_history.append(controller_lat)
        a_history.append(controller_long)
        ve_history.append(e)

        # Forward the controller command to the vehicle and update the status
        state = vehicle_model.single_track_model(controller_lat, controller_long)

        # Stop simulation if the end of the waypoints is reached
        track_end = False
        if pid_index == trajectory.shape[0]:
            track_end = True

        if track_end:
            # Save the simulation results in csv file
            results = np.transpose(
                np.asarray([x_history, y_history, yaw_history, v_history, a_history, delta_history]))
            pd.DataFrame(results).to_csv(os.path.join(path_to_results, 'simulation_results.csv'), index_label="Index",
                                         header=['x_history', 'y_history', 'yaw_history', 'v_history', 'a_history',
                                                 'delta_history'])
            pd.DataFrame(trajectory).to_csv(os.path.join(path_to_results, 'reference_trajectory.csv'),
                                            index_label="Index",
                                            header=['x', 'y', 'v'])
            pd.DataFrame(reference).to_csv(os.path.join(path_to_results, 'target.csv'), index_label="Index",
                                           header=['delta', 'a'])

            # Plot simulation results
            plot_history(x_history, y_history, v_history, ve_history, a_history, delta_history, used_index, pid_indexs, trajectory, reference)
            break

        if show_animation:
            if first_time:
                plt.plot(trajectory[:, 0], trajectory[:, 1], 'r-')
            plot_vehicle(current_x, current_y, steps)
