import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import os
from ncps.wirings import NCP, AutoNCP
from ncps.torch import LTC
import torch

from libs import training_data
from libs import vehicle_model


if __name__ == '__main__':
    # Path
    path_to_results = os.path.join("results", "final_test")  # create folder first

    # Gaimersheim
    trajectory1 = os.path.join("data", "waypoints_20180810150607_bus_signals.csv")
    reference1 = os.path.join("data", "reference_20180810150607_bus_signals.csv")
    # Ingolstadt
    trajectory2 = os.path.join("data", "waypoints_20190401145936_bus_signals.csv")
    reference2 = os.path.join("data", "reference_20190401145936_bus_signals.csv")
    # München
    trajectory3 = os.path.join("data", "waypoints_20190401121727_bus_signals.csv")
    reference3 = os.path.join("data", "reference_20190401121727_bus_signals.csv")

    # Routes
    trajectory_training = [trajectory1, trajectory2, trajectory3]
    reference_training = [reference1, reference2, reference3]

    trajectories, trajectories_normalized, references, references_normalized, max_values = \
        training_data.training_data(trajectory_training, reference_training, path_to_results)

    data_x = torch.from_numpy(np.expand_dims(trajectories_normalized[:9000], axis=0)).double()
    data_y = torch.from_numpy(np.expand_dims(references_normalized[:9000], axis=0)).double()
    val_x = torch.from_numpy(np.expand_dims(trajectories_normalized[9000:10000], axis=0)).double()
    val_y = torch.from_numpy(np.expand_dims(references_normalized[9000:10000], axis=0)).double()
    test_x = torch.from_numpy(np.expand_dims(trajectories_normalized[10000:], axis=0)).double()
    test_y = torch.from_numpy(np.expand_dims(references_normalized[10000:], axis=0)).double()

    trajectory_test = trajectories[10001:-1]
    reference_test = references[10001:-1]

    trajectory = trajectory_test
    reference = reference_test

    # Initialize list to save values
    x_history = []
    y_history = []
    yaw_history = []
    v_history = []

    # Path to logs of the training
    path_to_logs = os.path.join("results", "NCP_training")
    # Path to results folder
    # if not existing: please create the folder "NCP_vehicle_model" within the results folder
    path_to_results = os.path.join("results", "NCP_vehicle_model")

    numb_neurons, lr, grad_clip = 32, 0.01, 1.0  # have to be the same parameters as in the log file
    # Path to the logs of the training with the network hyperparameter
    path_to_model = os.path.join(path_to_logs, "log_{}_{}_{}".format(numb_neurons, lr, grad_clip), "lightning_logs",
                                 "version_0", "checkpoints", "epoch=2-step=3.ckpt")  # last entry may have to adapted depending on the number of epochs trained

    # Initialize the model (same values as used for the training)
    in_features = 6
    out_features = 2
    wiring = AutoNCP(numb_neurons, out_features)
    ltc_model = LTC(in_features, wiring, batch_first=True)

    # Load state dict (network parameter)
    checkpoint = torch.load(path_to_model)

    # Changes on state_dict have to be made to be able to load the state_dict in the model
    state_dict = torch.load(path_to_model)["state_dict"]
    # Create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[6:]  # remove `module.`
        new_state_dict[name] = v

    # Load parameter
    ltc_model.load_state_dict(new_state_dict)

    # Predictions with test data
    with torch.no_grad():
        prediction = ltc_model(test_x)[0]
    prediction = torch.squeeze(prediction)
    prediction = prediction.numpy()

    # Re-normalize delta and a
    delta = prediction[:, 0] * max_values[4] / 15.8  # Divide by 15.8 to get the steering angle at the wheel
    acceleration = prediction[:, 1] * max_values[5]

    # Forward the controller command to the vehicle and update the status
    for d, a in zip(delta, acceleration):
        state = vehicle_model.single_track_model(d, a)

        # Update current position
        current_x, current_y, current_yaw, current_v = state[0], state[1], state[2], state[3]

        # Save history
        x_history.append(current_x)
        y_history.append(current_y)
        yaw_history.append(current_yaw)
        v_history.append(current_v)

    # Save results to .csv
    results = np.transpose(np.asarray([x_history, y_history, yaw_history, v_history, acceleration, delta]))
    pd.DataFrame(results).to_csv(os.path.join(path_to_results, 'prediction_results.csv'), index_label="Index",
                                 header=['x_history', 'y_history', 'yaw_history', 'v_history', 'a_history', 'delta_history'])
    pd.DataFrame(trajectories).to_csv(os.path.join(path_to_results, 'reference_trajectory.csv'), index_label="Index",
                                      header=['x', 'y', 'v'])
    pd.DataFrame(references).to_csv(os.path.join(path_to_results, 'target.csv'), index_label="Index",
                                    header=['delta', 'a'])

    def plot_history(x_history, y_history, yaw_history, v_history, acceleration, delta, trajectory, reference):
        x_max = len(x_history)
        t_plot = np.arange(0, x_max / 10, 0.1)

        plt.clf()
        plt.rcParams['font.size'] = 10
        plt.figure(1, figsize=(12, 4))
        plt.plot(trajectory[:, 0], trajectory[:, 1], label='Target')
        plt.plot(x_history, y_history, label='Calculated')
        plt.title('Path')
        plt.grid()
        plt.legend()
        plt.xlabel("x " r"$[m]$")
        plt.ylabel("y " r"$[m]$")
        plt.tight_layout()
        plt.savefig(os.path.join(path_to_results, "path_nnc.pdf"), dpi=900,
                    format='pdf')

        plt.clf()
        plt.rcParams['font.size'] = 10
        plt.figure(2, figsize=(6, 4))
        plt.plot(t_plot, trajectory[:, 2], label='Target')
        plt.plot(t_plot, v_history, label='Calculated')
        plt.title('Velocity')
        plt.legend()
        plt.grid(True)
        plt.xlim([0, x_max / 10])
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("Velocity " r'$[m/s]$')
        plt.tight_layout()
        plt.savefig(os.path.join(path_to_results, "v_nnc.pdf"), dpi=900,
                    format='pdf')

        plt.clf()
        plt.rcParams['font.size'] = 10
        plt.figure(3, figsize=(6, 4))
        plt.plot(t_plot, reference[:, 1], label='Target')
        plt.plot(t_plot, acceleration, label='Prediction')
        plt.title('Acceleration')
        plt.legend()
        plt.grid(True)
        plt.xlim([0, x_max / 10])
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("Acceleration " r'$[m/s^2]$')
        plt.tight_layout()
        plt.savefig(os.path.join(path_to_results, "a_nnc.pdf"), dpi=900,
                    format='pdf')

        error_a = np.abs(reference[:, 1]) - np.abs(acceleration)

        t = np.arange(0, 250 / 10, 0.1)
        plt.rcParams['font.size'] = 10
        plt.figure(1, figsize=(12, 4))
        fig, (ax1, ax2) = plt.subplots(2, sharex=True)
        fig.suptitle('Acceleration', x=0.56)
        fig.tight_layout(pad=2.0)
        fig.subplots_adjust(top=0.6)
        ax1.plot(t, reference[0:250, 1], label="Target")
        ax1.plot(t, acceleration[0:250], label="Prediction")
        ax2.plot(t, error_a[:250])
        ax1.grid()
        ax2.grid()
        ax1.set_xlim([0, 25])
        ax2.set_xlim([0, 25])
        ax1.tick_params(axis="y")
        ax2.tick_params(axis="x")
        ax2.tick_params(axis="y")
        ax2.set_xlabel("Time [s]")
        ax1.set_ylabel("Acceleration " r'$[m/s^2]$')
        ax2.set_ylabel("Error "r'$[-]$')
        ax1.legend(loc="upper right", ncol=1)
        plt.tight_layout()
        fig.savefig(os.path.join(path_to_results,
                                 "section_prediciton_error_a_test_x_{}_{}_{}.pdf".format(numb_neurons, lr, grad_clip)),
                    dpi=900, format='pdf', bbox_inches='tight')

        plt.clf()
        plt.rcParams['font.size'] = 10
        plt.figure(4, figsize=(6, 4))
        plt.plot(t_plot, reference[:, 0] / 15.8, label='Target')
        plt.plot(t_plot, delta, label='Prediction')
        plt.title('Steering Angle')
        plt.legend()
        plt.grid(True)
        plt.xlim([0, x_max / 10])
        plt.xlabel("Time " r"$[s]$")
        plt.ylabel("Steering Angle " r'$[rad]$')
        plt.tight_layout()
        plt.savefig(os.path.join(path_to_results, "delta_nnc.pdf"), dpi=900,
                    format='pdf')

    # Plot the simulation results
    plot_history(x_history, y_history, yaw_history, v_history, acceleration, delta, trajectory, reference)


