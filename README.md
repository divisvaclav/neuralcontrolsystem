# Tracking Control of an Autonomous Vehicle by a Neural Network Controller 

## What is the Project about?

The neural network model of Lechner et al. (NCP Model) is used as a neural network controller for tracking control of an autonomous vehicle. Therefore, the network is trained with data from recorded drives with a real car. A vehicle model is implemented as well to test the prediction results of the NCP model on a virtual vehciel.
To have a comparsion basis and to better access the simulation results a PID and Stanley controller are implemented too. 

## Important Links

- [Neural Circuit Policies Enabling Auditable Autonomy by Lechnet et al.](https://publik.tuwien.ac.at/files/publik_292280.pdf)
- [NCP GitHub Repository](https://github.com/mlech26l/ncps)
- [Google Colab (Pytorch) Basic Usage](https://colab.research.google.com/drive/1VWoGcpyqGvrUOUzH7ccppE__m-n1cAiI?usp=sharing) 

## Necessary Installation:

ncps is necessary to be able to work with the NCP model.

```bash
pip install ncps
```

In this case pytorch was used and torch and pytorch_lightning has to be installed.

```bash
pip install torch pytorch_lightning
```
For the preprocessing of the data utm is needed which is used to convert GPS to cartesian.
```bash
pip install utm
```

Furthermore, matplotlib, seaborn and pandas are used for visualisation and to save the results. All requirements are listed in the "requirement.txt" file.

Project created with Python 3.7.

## Folder Structure
The folder structre has to be as following:

    .
    ├── libs                   		        # Source files
    ├── data                   		        # Data for training und simulation
    ├── results                   		        # Results folder
    ├── plotting                   		        # Scripts for plotting the results
    ├── main_ncp_training.py                        # Training of the NCP model
    ├── main_ncp_vehicl_model.py		        # Simulation of the trained ncp model together with the vehicle model
    ├── main_pid_stanley_vehicl_model.py            # Simulattion of the PID and Stanley controller together with the vehicle model
    ├── main_ncp_training_reproducibility.py        # Training ot the NCP model with the same setting several times 
    └── README.md

The libs folder contrains all source files as the script for preprocessing the data, the vehicle model and the PID and Stanley controller. 
It also contains the file "example.py" where a simple example from the ncp repository is given.

To plot the results (loss, prediction, simulation results) multiple scripts are provided in the plotting folder.

The folders to save the training results (subfolders within results) and the plots (subfolders within plotting) have to be created as well as the folder for the data.
    
## Data

The data is taken from [A2D2](https://www.a2d2.audi/a2d2/en.html). The recorded bus signals of three cities is stored in a .json files, which are needed to run the code. The .json files have to be placed in the data folder.
The data .json files and the preprocessed data (.csv) is also available here: [LPL-sharepoint](https://lplcloud-my.sharepoint.com/personal/akhil_sathuluri_lpl-tum_com/_layouts/15/onedrive.aspx?ga=1&id=%2Fpersonal%2Fakhil%5Fsathuluri%5Flpl%2Dtum%5Fcom%2FDocuments%2FAkhil%2F04%5FStudents%2F01%5FPID2NN)

## Usage - Explanation of the Files

### Preprocessing of the Data

The data is preprocessed in the file "signals_a2d2.py" where the singals are interpolated and converted to the correct unit. This file has to be run once to get a .csv file with the trainjectory and reference data for each drive. The plotting of the preprocessed data is included (default: plot_data = False). 

### Data Preparation for Training the NCP Model

The file "training_data.py" prepares the data for the training of the ncp model. Thereby the signals are normalized. This script is called automatically in "main_ncp_training.py". Plotting of the training data can be acctivated with plot_data = True (default: False). 

### Training the NCP Model

For the training of the NCP model the file "main_NCP_training.py" has to be run. Within this file the training data is split up into training, validation and test. The loss (training, validation and test) and hyperparameter are logged and saved in the result.
It is possible to perform a grid search with: number of neurons, learning rate and gradient clipping value instead of specifying one value for each parameter.

 After having a suitable parameters found, "main_ncp_training_reproducibility.py" can be used to test the reproducibility of the results. The script is built up in the same way as "main_NCP_training.py".

### Simulation with Vehicle Model

 "main_ncp_vehicl_model.py" is for testing the predictions of the trained model with the test data on the vehicle model. The parameters of the trained model are load and the predictions are forwarded to the vehicle model. The state of the vehicle model is updated after every time step. The results and plots are saved.
 
 Next to the simulation of the vehicle model with the ncp controller, the vehicle model can also be run with a PID and Stanley controller ("main_pid_stanley_vehicl_model.py"). The calculated control outputs are forwarded to the vehicle model and the results and plots are saved. 
 
 ### Additional Plotting Scripts 
 
 "test_data_section.py": plot section of the test data for the PID and Stanley controller, section of training data for NCP model or NCP network architecture
 
 "loss.py": plot the training and validation loss and save the test loss in seperate csv (for one or also more models)
 
 "loss_comparison.py": compare the training and validation loss of three (can also be extended) models (additional: zoom-in plot)
 
 "test_data_prediction_results.py": plot predictions (delta, acceleration) of the NCP model on the test data (different plots possible) 
 
"box_plot_reproducibiltiy.py": visualize the results after running the training with one setting serval times. 
 
"comparison_controller.py": compare the simulation results (steering angle, acceleration) of the PID and Stanley controller with the NCP controller 
 
 "steering_angle_comparison": compare the predicted steering angle of the NCP model with different parameter settings
 
 ### Further Notes
  
 Before usage, please check if:
 - the folders (to save the results and place the data) exist 
 - the .json file is placed in the data folder
 - the name of the checkpoint file is correct (name changes corrding to number of training epochs)
 
 
 
 
 
 
